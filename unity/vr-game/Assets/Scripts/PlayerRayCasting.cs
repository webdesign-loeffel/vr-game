﻿using UnityEngine;
using System.Collections;
using System.IO;
using SimpleJSON;

public class PlayerRayCasting : MonoBehaviour {

	public float distance;
	public bool debug;
	private RaycastHit hit;

	private string loadQuestionFile(string path = "Config/questions"){
		TextAsset asset = Resources.Load(path) as TextAsset;
		return asset.text;
	}

	private void print(string value = ""){
		Debug.Log(value);
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		/**
		 * Debug
		 */
		if(this.debug) Debug.DrawRay(transform.position, transform.forward, Color.black, this.distance);
		
		/**
		 * RayCasting
		 */
		Ray raydirection = new Ray(transform.position, transform.forward);
		if (Physics.Raycast(raydirection, out hit, this.distance)){

			/**
			 * QuestionBox tagged objects
			 */
			if(hit.collider.tag == "QuestionBox"){
				
				this.print("Press F");

				if(Input.GetKeyDown(KeyCode.F)){

					/**
					 * Parse JSON
					 */
					var Questions = JSON.Parse(this.loadQuestionFile()); /** Need optimization */
					if(Questions["geo"][(string)hit.collider.gameObject.name] != null){
						/**
						 * Return informations
						 */
						
						string result = "Frage: " + Questions["geo"][(string)hit.collider.gameObject.name]["question"] + "\n";
						result+= "Antwort: " + Questions["geo"][(string)hit.collider.gameObject.name]["answer"] + "\n";
						result+= "Richtung: " + Questions["geo"][(string)hit.collider.gameObject.name]["arrow"];

						this.print(result);
					}
				}

			}

		}
	}
}
