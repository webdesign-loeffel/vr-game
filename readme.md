# VR Game
Lucas Löffel  
Hochschule Worms - Inf2171  
Matr.-Nr.: 669097

## Dokumentation
Die Dokumentation zu diesem Projekt finden Sie unter `/_doku/VR-Doku-InDesign/doku.pdf`.

## Repo
Dieses Repository enthält alle Files und ist öffentlich zugänglich. Es wurden keine Files per `.gitignore-File` ausgeschlossen.

## Screenshots
Für ein besseres Verständnis habe ich noch einige Screenshosts angehängt. Diese sind unter `/screenshots` zu finden.

## Kontakt
Bei Fragen können Sie mich jederzeit unter folgenden Kontaktinformationen erreichen:

```
E-Mail: lucas.loeffel@gmail.com
Telefon: 0176 2418 9059
```