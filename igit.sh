#!/bin/bash
if [ ! -z "$1" ]
then
	git add --all
	git commit --all -m "$1"
	git push origin master
else 
	echo "Error: invalid message"
fi
